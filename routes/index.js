import { fetchPullRequestActivity } from '../lib/pull-request/fetcher';
import { getDiffUrl } from '../lib/pull-request/entities';
import fs from 'fs';

export default function routes(app, addon) {
    //healthcheck route used by micros to ensure the addon is running.
    app.get('/healthcheck', function(req, res) {
        res.send(200);
    });

    // Root route. This route will redirect to the add-on descriptor: `atlassian-connect.json`.
    app.get('/', function (req, res) {
        res.format({
            // If the request content-type is text-html, it will decide which to serve up
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            // This logic is here to make sure that the `atlassian-connect.json` is always
            // served up when requested by the host
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

    // Route to receive command from Slack and register user ID (Bitbucket and Slack) on database
    app.post('/register', async function (req, res) {
        const body = req.body;
        const mapData = await getFromDatabase();

        if(!mapData.get(body.text)) {
            fs.appendFile('bitbucket_slack.txt', `${body.text}:${body.user_id}\n`, function(err) {
                if (err) {
                    res.sendStatus(500);
                } else {
                    res.send('GG');
                }
             });
        } else {
            res.send('Você já está cadastrado.');
        }
    });

    app.get('/pull-request-panel', addon.authenticate(), function (req, res) {
        // Gets a decorated HTTP client ready to perform JWT-authenticated requests
        var httpClient = addon.httpClient(req);

        const repoPath = req.query.repoPath;
        const prId = req.query.prId;
        const userId = req.query.user;

        fetchPullRequestActivity(httpClient, repoPath, prId)
            .then(prActivity => {
                const userHasApproved = !!prActivity.lastApprovalByUserId[userId];
                const lastApprovalDiffUrl = getDiffUrl(prActivity, prActivity.lastApprovalByUserId[userId]);
                let lastCommentDiffUrl = getDiffUrl(prActivity, prActivity.lastCommentByUserId[userId]);
                if (lastCommentDiffUrl === lastApprovalDiffUrl) {
                    lastCommentDiffUrl = null;
                }

                if (prActivity.outdatedApprovals.length > 0) {
                    const outdatedApprovers = prActivity.outdatedApprovals
                        .map(approval => approval.user.displayName)
                        .join(', ');

                    res.render('pull-request-card', {
                        outdatedApprovers: outdatedApprovers,
                        approvalDiffUrl: lastApprovalDiffUrl,
                        approvalDiffUrlClass: lastApprovalDiffUrl ? '' : 'hide',
                        commentDiffUrl: lastCommentDiffUrl,
                        commentDiffUrlClass: lastCommentDiffUrl ? '' : 'hide',
                    });
                } else if (!userHasApproved && lastCommentDiffUrl) {
                    res.render('pull-request-card-diff-only', {
                        commentDiffUrl: lastCommentDiffUrl,
                        commentDiffUrlClass: lastCommentDiffUrl ? '' : 'hide',
                    });
                } else {
                    res.render('empty');
                }
            })
            .catch(err => {
                console.error('Error on /pull-request-panel', err);
                res.sendStatus(500);
            })
    });

    app.get('/slack-integration', addon.authenticate(), function (req, res) {
        const userAccountId = req.query.userAccountId;
        res.render('slack-integration', { userAccountId });
    });

    app.get('/pull-requests-stats', addon.authenticate(), async function (req, res) {
        var httpClient = addon.httpClient(req);

        console.log('Antes');
        const workspaces = await getAllFromPagination(
            httpClient,
            `/2.0/workspaces`,
            jsonData => jsonData.values,
        );

        const mainWorkspace = workspaces.map(result => result.slug)[0]
        const repos = await getAllFromPagination(
                httpClient,
                `/2.0/repositories/${mainWorkspace}?pagelen=100`,
                jsonData => jsonData.values,
        )

        const desiredRepos = new Set(['channels-api', 'api', 'webnext']);
        const repoSlugs = repos
            .filter(repo => desiredRepos.has(repo.slug))
            .map(repo => `${mainWorkspace}/${repo.slug}`);

        const statByPrId = {};
        for (const repoSlug of repoSlugs) {
            const results = await getAllFromPagination(
                httpClient,
                `/2.0/repositories/${repoSlug}/pullrequests/activity?pagelen=50`,
                jsonData => jsonData.values,
                10,
                1
              );

            results.forEach(activity => {
                const prId = `${repoSlug}-${activity.pull_request.id}`;
                if (!statByPrId[prId]) {
                    statByPrId[prId] = {
                        merged: false,
                        updates: 0,
                        outdatedApprovalCount: 0,
                        validApprovalCount: 0
                    };
                }

                if (activity.update) {
                    statByPrId[prId].updates++;
                    if (activity.update.state === 'MERGED') {
                        statByPrId[prId].merged = true;
                    } else {
                        if (statByPrId[prId].merged && !statByPrId[prId].approved) {
                            statByPrId[prId].invalidMerge = true;
                        }
                    }
                } else if (activity.approval) {
                    statByPrId[prId].approved = true;
                    if ((!statByPrId[prId].merged && statByPrId[prId].updates > 0) ||
                        (statByPrId[prId].merged && statByPrId[prId].updates > 1)) {
                        statByPrId[prId].outdatedApprovalCount++;
                    } else {
                        statByPrId[prId].validApprovalCount++;
                    }
                }
            });
        }

        let merged = 0;
        let invalidMerged = 0;
        let approved = 0;
        let hasValidApprovals = 0;
        let hasOutdatedApprovals = 0;
        for (const prId in statByPrId) {
            if (statByPrId[prId].invalidMerge) {
                invalidMerged++;
            }
            if (statByPrId[prId].merged) {
                merged++;
            }
            if (statByPrId[prId].approved) {
                approved++;
            }
            if (statByPrId[prId].validApprovalCount > 0) {
                hasValidApprovals++;
            }
            if (statByPrId[prId].outdatedApprovalCount > 0) {
                hasOutdatedApprovals++;
            }
        }

        console.log(`Full stat: ${JSON.stringify(statByPrId)}`);
        console.log(`Total Repositories: ${repoSlugs.length}`);
        console.log(`Total PRs: ${Object.keys(statByPrId).length}`);
        console.log(`Merged: ${merged}`);
        console.log(`Invalid merges: ${invalidMerged}`);
        console.log(`Has valid approvals: ${hasValidApprovals}`);
        console.log(`Has invalid approvals: ${hasOutdatedApprovals}`);

        res.render('slack-integration', { });
    });

    // This route will handle webhooks from repositories this add-on is installed for.
    // Webhook subscriptions are managed in the `modules.webhooks` section of
    // `atlassian-connect.json`

    app.post('/webhook', addon.authenticate(), async function (req, res) {
        const bodyData = req.body.data;

        if (!bodyData.repository || !bodyData.pullrequest) {
            return;
        }

        const repoPath = bodyData.repository.full_name;
        const prId = bodyData.pullrequest.id;

        var httpClient = addon.httpClient(req);

        const prActivities = await fetchPullRequestActivity(httpClient, repoPath, prId);

        if (prActivities && prActivities.outdatedApprovals) {
            const usersToSendNotification = prActivities.outdatedApprovals.map(approval => approval.user.accountId);

            const prUrl = `https://bitbucket.org/${repoPath}/pull-requests/${prId}`;
            sendSlackMessage(httpClient, usersToSendNotification, prUrl);
        }

        res.sendStatus(204);
    });

    async function sendSlackMessage(httpClient, bitbucketIds, prUrl) {
        const mapData = await getFromDatabase();

        bitbucketIds.forEach(bitbucketId => {
            const slackId = mapData.get(bitbucketId);

            httpClient.post({
                url: 'https://slack.com/api/chat.postMessage',
                headers: {
                    'Content-type': 'application/json',
                    'Authorization': 'Bearer xoxb-1203289233975-1230674623537-z8smBk7JbMiqFUGlQEzMVYY2'
                },
                body: JSON.stringify({
                    channel: slackId,
                    text: `Um PR que você aprovou foi alterado!\n${prUrl}`
                })
            },
            function () {});
        });
    }

    function getFromDatabase() {
        return new Promise((resolve, reject) => {
            fs.readFile('bitbucket_slack.txt', function (err, data) {
                const mapData = new Map();
                const content = data.toString();
                const lines = content.split('\n');

                if (!lines) {
                    resolve(mapData);
                }

                lines.forEach((line) => {
                    const keyValue = line.split(':');
                    mapData.set(keyValue[0], keyValue[1]);
                });

                if (err) {
                    reject(err);
                }

                resolve(mapData);
            });
        });
    }

};
