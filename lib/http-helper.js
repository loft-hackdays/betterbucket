export async function getAllFromPagination(httpClient, url, payloadTransformer) {
  return new Promise((resolve, reject) => {
    const allResults = [];

    httpClient.get(url, function (err, resp, data) {
      if (err) {
        reject(err);
      }

      try {
        const jsonData = JSON.parse(data);
        allResults.push(...payloadTransformer(jsonData));

        const nextUrlToFecth = jsonData.next;
        if (!nextUrlToFecth) {
          resolve(allResults);
        } else {
          getAllFromPagination(httpClient, nextUrlToFecth, payloadTransformer)
            .then(res => {
              allResults.push(...res)
              resolve(allResults);
            })
            .catch(error => reject(error));
        }
      } catch (error) {
        reject(error);
      }
    });
  });
}
