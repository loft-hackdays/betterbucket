import { getAllFromPagination } from "../http-helper";
import { PullRequestActivity } from "./entities";

export async function fetchPullRequestActivity(httpClient, repoPath, prId) {
  const results = await getAllFromPagination(
    httpClient,
    `/2.0/repositories/${repoPath}/pullrequests/${prId}/activity?pagelen=50`,
    jsonData => jsonData.values,
  );

  return new PullRequestActivity(results, repoPath);
}