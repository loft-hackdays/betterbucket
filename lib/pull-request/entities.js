export class User {
  constructor(userObj) {
    this.uuid = userObj.uuid;
    this.displayName = userObj.display_name;
    this.nickname = userObj.nickname;
    this.accountId = userObj.account_id;
  }
}

export class Approval {
  // This is the latest update approved by this approval
  lastUpdateBefore;

  constructor(approvalObj, prActivity) {
    this.user = new User(approvalObj.user);
    this.date = new Date(approvalObj.date);
    this.prId = approvalObj.pullrequest.id;
    this.prActivity = prActivity;
  }

  isOutdated() {
    return this.lastUpdateBefore !== this.prActivity.latestUpdate;
  }
}

export class Comment {
  // This is the last update that happened before this comment
  lastUpdateBefore;

  constructor(commentObj) {
    this.user = new User(commentObj.user);
    this.date = new Date(commentObj.updated_on);
    this.prId = commentObj.pullrequest.id;
  }
}

export class Update {
  constructor(updateObj) {
    this.commitHash = updateObj.source.commit.hash;
    this.author = new User(updateObj.author);
    this.date = new Date(updateObj.date);
  }
}

export class PullRequestActivity {

  get validApprovals() {
    return this.approvals.filter(approval => !approval.isOutdated());
  }

  get outdatedApprovals() {
    return this.approvals.filter(approval => approval.isOutdated());
  }

  constructor(activityList, repoPath) {
    this.repoPath = repoPath;
    this.latestUpdate = undefined;

    this.approvals = [];
    this.approvalsByUserId = {};

    // An interaction is either a comment or an approval. The interactionsPendingUpdate
    // is used so we mark the last update that happened before a given interaction
    this.lastApprovalByUserId = {};
    this.lastCommentByUserId = {};
    let interactionsPendingUpdate = [];

    activityList.forEach(activityObj => {
      if (this.isUpdate(activityObj)) {
        const update = new Update(activityObj.update);
        if (!this.latestUpdate) {
          this.latestUpdate = update;
        }

        interactionsPendingUpdate.forEach(interaction => interaction.lastUpdateBefore = update);
        interactionsPendingUpdate = [];
      }

      if (this.isApproval(activityObj)) {
        const approval = new Approval(activityObj.approval, this);
        this.approvals.push(approval);
        this.approvalsByUserId[approval.user.uuid] = approval;

        this.registerInteraction(approval, this.lastApprovalByUserId);
        interactionsPendingUpdate.push(approval);
      }

      if (this.isComment(activityObj)) {
        const comment = new Comment(activityObj.comment);

        this.registerInteraction(comment, this.lastCommentByUserId);
        interactionsPendingUpdate.push(comment);
      }
    });
  }

  registerInteraction(interaction, interactionMap) {
    // If there is no interaction for this user, register it as being the last one
    if (!interactionMap[interaction.user.uuid]) {
      interactionMap[interaction.user.uuid] = interaction;
    }
  }

  isUpdate(activityObj) {
    return activityObj.update;
  }

  isApproval(activityObj) {
    return activityObj.approval;
  }

  isComment(activityObj) {
    return activityObj.comment;
  }
}

export function getDiffUrl(prActivity, lastInteraction) {
  const lastUpdateBeforeInteraction = lastInteraction && lastInteraction.lastUpdateBefore;

  if (lastUpdateBeforeInteraction && lastUpdateBeforeInteraction !== prActivity.latestUpdate) {
    return `https://bitbucket.org/${prActivity.repoPath}/branches/compare/`
      + `${prActivity.latestUpdate.commitHash}..${lastUpdateBeforeInteraction.commitHash}`
      + '?w=1&ts=2#diff';
  }

  return null;
}
